//Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and 
//return it in an array in numeric values. [111, 139, 161, 143].


let ip = "111a.139.161.143"

const splittingIpAddress = (ip) => {



let result = []

if(ip == undefined) return "Enter valid string"

if(Object.prototype.toString.call(ip) !== '[object String]') return "Not a  String"

let arr = ip.split(".")

for (i =0 ; i < arr.length; i++){
    if(isNaN(arr[i]) || arr[i] <0 || arr[i] > 255){
       
        return []
        
        
    }else{
       
         result.push(parseFloat(arr[i]))
    }
}

return result

}

//console.log(splittingIpAddress(ip))

module.exports = splittingIpAddress