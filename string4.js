// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}




let names = ["first_name","middle_name","last_name"]

const getFullName = (obj) => {

let fullName = ""

for(let each of names){
   // console.log(each)
    if(obj[each]){
       
      fullName += obj[each][0].toUpperCase() + obj[each].slice(1).toLowerCase() + " "
    }
}




// for (i=0; i < objValues.length ;  i++){

//     let name = objValues[i]

// let titleCasedName = name[0].toUpperCase() + name.slice(1).toLowerCase()

//     obj[i] = titleCasedName


// }

return fullName

}


module.exports = getFullName